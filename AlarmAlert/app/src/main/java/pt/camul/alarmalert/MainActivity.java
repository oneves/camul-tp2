package pt.camul.alarmalert;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnStart;
    private Button btnStop;

    Intent intent;
    PendingIntent pIntent = null;
    AlarmManager alarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnStart = (Button)findViewById(R.id.btnStart);
        btnStop = (Button)findViewById(R.id.btnStop);

        setButtonsVisible();

        btnStart.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        scheduleAlarm();
                        setButtonsVisible();
                    }
                }
        );

        btnStop.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        if(pIntent != null) {
                            alarm.cancel(pIntent);
                            pIntent.cancel();
                            pIntent = null;
                            setButtonsVisible();
                        }
                    }
                }
        );
    }

    private void setButtonsVisible() {
        if(pIntent != null)
        {
            btnStart.setEnabled(false);
            btnStop.setEnabled(true);
        }
        else
        {
            btnStart.setEnabled(true);
            btnStop.setEnabled(false);
        }
    }

    public void scheduleAlarm() {
        // Construct an intent that will execute the AlarmReceiver
        intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        // Create a PendingIntent to be triggered when the alarm goes off
        pIntent = PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        // Setup periodic alarm every 5 seconds
        long firstMillis = System.currentTimeMillis(); // alarm is set right away
        alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        // First parameter is the type: ELAPSED_REALTIME, ELAPSED_REALTIME_WAKEUP, RTC_WAKEUP
        // Interval can be INTERVAL_FIFTEEN_MINUTES, INTERVAL_HALF_HOUR, INTERVAL_HOUR, INTERVAL_DAY
        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis,
                20000, pIntent); // 20 segundos
    }

}
