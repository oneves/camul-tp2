package pt.camul.alarmalert;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Alexandre Bastos | Francisco Fernandes | Orlando Neves on 21/05/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 12345;

    // Triggered by the Alarm periodically (starts the service to run task)
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, DetectAlarmService.class);
        i.putExtra("foo", "bar");
        context.startService(i);
    }
}
