package pt.camul.alarmalert;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;
import android.os.Vibrator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Timer;

/**
 * Created by Alexandre Bastos | Francisco Fernandes | Orlando Neves on 21/05/2016.
 */
public class DetectAlarmService extends IntentService {
    public DetectAlarmService() {
        super("DetectAlarmService");
        Log.i("DetectAlarmService", "Service running...");
    }


    private static int sampleRate = 10000;
    private FFT fft;

    @Override
    protected void onHandleIntent(Intent intent) {
        // Do the task here
        if (isAlarm()) {

            Intent myIntent = new Intent(this, AlertActivity.class);
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            myIntent.setType("text/plain");
            myIntent.putExtra("isAlarm", "1");
            startActivity(myIntent);

            Log.i("DetectAlarmService", "Enviar alerta");
        }
    }



    public boolean isSoundCapured()
    {
        boolean recorder=true;
        int calculateRepetitions=0;
        List<Float> allFreq = new ArrayList<>();
        List<Float> rateFreq = new ArrayList<>();

        List<Integer> indexes = new ArrayList<>();
        Map<Float, Integer> freqCount = new HashMap<>();

        int minSize = 1024;//AudioRecord.getMinBufferSize(44100, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        AudioRecord ar = new AudioRecord(MediaRecorder.AudioSource.MIC, 8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT,1024 * 2);

        short[] buffer = new short[minSize];

        ar.startRecording();
        int countaa = 0;
        while(recorder)
        {

            final int bufferReadResult = ar.read(buffer, 0, minSize);

            float frequency = calculateFrequency(8000, buffer);
            float max = 0;

            allFreq.add(frequency);

            //Log.i("Frequency", ""+frequency);


            if(frequency>=1000 && frequency<=3000){
                rateFreq.add(frequency);

                countaa++;

                Log.i("CONTAGEM -->", ""+countaa);
                Log.i("Frequency", ""+frequency);
                //ar.stop();
                //return true;

            }

            if(rateFreq.size()>= 50){
                ar.stop();
                for(Float f: rateFreq){

                    indexes.add(allFreq.indexOf(f));

                    int count=0;
                    for(int j=0; j<rateFreq.size(); j++){

                        if(rateFreq.get(j) >= f-50 &&  rateFreq.get(j)<= f+50){
                            count++;
                        }
                    }
                    freqCount.put(f, count);
                }

                int maxVal=0;
                for(Integer val: freqCount.values()){
                    if(val>maxVal) {
                        maxVal = val;
                    }
                }

                float mostRepeatedFreq = getKeysByValue(freqCount, maxVal);

                Log.i("MOST REPEATED FREQ", ""+mostRepeatedFreq);

                List <Integer> indexOfRepeatedFreq = new ArrayList<>();

                for(int k=0; k<rateFreq.size(); k++){

                    if(rateFreq.get(k)>= mostRepeatedFreq-50 && rateFreq.get(k)<= mostRepeatedFreq+50){
                        indexOfRepeatedFreq.add(k);
                    }

                }

                boolean result = isAlarmDetected(indexOfRepeatedFreq);

                if(result){
                    ar.stop();
                    ar.release();
                }
                return result;
            }
        }

        ar.stop();
        ar.release();
        return false;

    }

    private boolean isAlarmDetected(List<Integer> values){

        List<Integer> periodRead = new ArrayList<>();
        boolean isOnMargin = false;
        for(int i=0; i<values.size(); i++ ){

            if(i< values.size()-1){
                periodRead.add(values.get(i+1)-values.get(i));
                Log.i("PERIOD", "" + (values.get(i + 1) - values.get(i)));
            }


        }

        int countTrue =0;
        int countFalse=0;

        int countJ;
        for(int i=0; i<periodRead.size()-3; i++){
            Log.i("COUNT-->", ""+countTrue);
            countTrue = 0;

            for(int j=i+1; j<periodRead.size()-3; j++){

                if(periodRead.get(i) == periodRead.get(j) && periodRead.get(i+1) == periodRead.get(j+1) &&
                        periodRead.get(i+2) == periodRead.get(j+2)){

                    Log.i("AQUIIIIIIII", "COMEÇAAAA");
                    Log.i("i","--j");
                    Log.i(""+periodRead.get(i), ""+periodRead.get(j));
                    Log.i(""+periodRead.get(i+1), ""+periodRead.get(j+1));
                    Log.i(""+periodRead.get(i+2), ""+periodRead.get(j+2));
                    /*Log.i(""+periodRead.get(i+3), ""+periodRead.get(j+3));
                    Log.i(""+periodRead.get(i+4), ""+periodRead.get(j+4));*/

                    Log.i("Indice i -->", ""+i);
                    Log.i("Indice j -->", ""+j);

                    countTrue++;
                }

                if(countTrue>3){
                    return true;
                }

                /*if(val+0>=periodRead.get(i) && val-0<=periodRead.get(i)){
                    //isOnMargin=true;
                    countTrue++;
                }else{
                    //return false;
                    countFalse++;
                }*/
            }
        }
        return false;
    }

   /* private boolean isAlarmDetected(List<Integer> values){

        List<Integer> periodRead = new ArrayList<>();
        boolean isOnMargin = false;
        for(int i=0; i<values.size(); i++ ){

            if(i< values.size()-1){
                periodRead.add(values.get(i+1)-values.get(i));
                Log.i("PERIOD", "" + (values.get(i + 1) - values.get(i)));
            }


        }

        List <Integer> trues = new ArrayList<>();
        List <Integer> falses = new ArrayList<>();
        int countTrue =0;
        int countFalse=0;
        for(Integer val: periodRead){


            for(int i=0; i<periodRead.size(); i++){

                if(val+0>=periodRead.get(i) && val-0<=periodRead.get(i)){
                    //isOnMargin=true;
                    falses.add(countFalse);
                    countFalse=0;
                    countTrue++;
                }else{
                    //return false;
                    trues.add(countTrue);
                    countTrue=0;
                    countFalse++;
                }
            }
        }


        return countTrue>countFalse;
    }*/

    public boolean compareValues(List<Integer> trues, List<Integer> falses){

        int trueValue =0;
        int falseValue=0;

        for (int i=1; i<trues.size() && i < falses.size(); i++){
            trueValue=trues.get(i);
            falseValue=falses.get(i);


        }
        return false;
    }

    public float getKeysByValue(Map<Float, Integer> map, Integer value) {

        float keys =0;
        for (Map.Entry<Float, Integer> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return keys;
    }

    public float calculateFrequency(int sampleRate, short [] audioData){

        int numSamples = audioData.length;
        int numCrossing = 0;
        for (int p = 0; p < numSamples-1; p++)
        {
            if ((audioData[p] > 0 && audioData[p + 1] <= 0) ||
                    (audioData[p] < 0 && audioData[p + 1] >= 0))
            {
                numCrossing++;
            }
        }

        float numSecondsRecorded = (float)numSamples/(float)sampleRate;
        float numCycles = numCrossing/2;
        float frequency = numCycles/numSecondsRecorded;

        return frequency;
    }




    private boolean isAlarm()
    {
        // Colocar aqui todo o código de identificação do padrão dos alarmes e devolver true or false se for alarme ou não.
        return isSoundCapured();
        //return true; //To test...
    }
}

